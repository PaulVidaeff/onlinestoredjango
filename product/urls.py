from django.urls import path

from product.views import ProductListView, ProductUpdateView, ProductDeleteView, ProductCreateView, ProductDetailView

urlpatterns = [
    path('product-list/', ProductListView.as_view(), name='product_list'),
    path('product-update/<int:pk>/', ProductUpdateView.as_view(), name='product_update'),
    path('product-delete/<int:pk>/', ProductDeleteView.as_view(), name='product_delete'),
    path('product-create/', ProductCreateView.as_view(), name='product_create'),
    path('product-details/<int:pk>/', ProductDetailView.as_view(), name='product_details')
]
