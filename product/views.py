from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from product.models import Product


class ProductListView(ListView):
    template_name = 'product/product_list.html'
    model = Product
    context_object_name = 'all_products'


class ProductCreateView(CreateView):
    template_name = 'product/product_create.html'
    model = Product
    fields = '__all__'
    success_url = reverse_lazy('product_list')


class ProductUpdateView(UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    fields = '__all__'
    success_url = reverse_lazy('product_list')


class ProductDeleteView(DeleteView):
    template_name = 'product/product_delete.html'
    model = Product
    success_url = reverse_lazy('product_list')


class ProductDetailView(DetailView):
    template_name = 'product/product_details.html'
    model = Product
    context_object_name = 'product_details'
