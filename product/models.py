from django.db import models


class Product(models.Model):
    product_name = models.CharField(max_length=50)
    product_quantity = models.TextField()
    product_price = models.TextField()
    price_currency = models.CharField(max_length=3)
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.product_name
